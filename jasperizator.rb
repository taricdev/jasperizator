Dir['./java/*.jar'].each do |jar|
  require jar
end

Dir['./source/**/*.jrxml'].each do |f|
  puts "Compilando: #{f}"
  Java::NetSfJasperreportsEngine::JasperCompileManager.compile_report_to_file(f, f.gsub(/.jrxml$/,'.jasper'))
end
