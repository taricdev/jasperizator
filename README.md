# Jasperizator

Compilador masivo de ficheros Jasper .jrxml

El proyecto está Dockerizado, para generar la imagen hay que ejecutar:

    docker build --tag taric/jasperizator .

Una vez generada la imagen ya se pueden lanzar contenedores para ejecutar el script:

    docker run -ti --rm -v "$PWD":/usr/src/jasperizator/source taric/jasperizator
