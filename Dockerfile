FROM ubuntu:16.04

RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

RUN apt-get update && apt-get -y install maven

RUN mkdir -p /usr/src/jasperizator

WORKDIR /usr/src/jasperizator

COPY pom.xml ./

RUN mvn process-sources

COPY jasperizator.rb ./

CMD java -jar ./java/jruby-complete.jar -S jasperizator.rb
